# find-text-service

A simple web-service for finding text on an image using an EAST alogorithm.

Rest Api
-----

POST - Searches for text on the image and returns a json containing the coordinates of the bounding boxes with text on the image: /process + pass file as image.

Example of the cURL command: `curl -i -X POST -F "image=@path/to/image/filemane.jpg" http://localhost:5000/process`

Docker
-----
This application can be run in Docker.  Please see Dockerfile for image setup.  Steps to create an image & how to run
the app in a container list below. (must have docker installed)

Create a docker image: `docker build -t find_text_service .`

Run docker container: `docker run -d -p 5000:5000 find_text_service`

Once app has started, it is possible to send POST requests to the [http://localhost:5000/process] endpoint.

