FROM ubuntu:18.04

RUN  useradd -m find_text_service
WORKDIR /home/find_text_service

COPY requirements.txt requirements.txt
RUN apt-get -yqq update
RUN apt-get -yqq install python3-pip python3-dev python3-venv
RUN python3 -m venv venv
RUN apt-get -yqq install libsm6 libxext6 libxrender1 libfontconfig1 libxext6
RUN venv/bin/pip3 install --upgrade pip
RUN venv/bin/pip3 install -r requirements.txt
RUN venv/bin/pip3 install gunicorn

COPY app app
COPY find_text_service.py config.py boot.sh ./
RUN chmod +x boot.sh

ENV FLASK_APP find_text_service.py

RUN chown -R find_text_service:find_text_service ./

EXPOSE 5000
ENTRYPOINT ["./boot.sh"]
