from imutils.object_detection import non_max_suppression
import numpy as np
import cv2

_BASE_INPUT_SIZE = 320


class TextDetectException(Exception):
    pass


def _prepare_image(image):
    """
    Prepare image for the EAST algorithm and create blob.
    :param image: bytes representation of the image.
    """
    image = cv2.resize(image, (_BASE_INPUT_SIZE, _BASE_INPUT_SIZE))
    return cv2.dnn.blobFromImage(image, 1.0, image.shape[:2], (123.68, 116.78, 103.94), swapRB=True, crop=False)


def _decode(scores, geometry, min_confidence):
    """
    Process detection results.
    :param scores: array that is used to derive the bounding box coordinates of text in our input images.
    :param geometry: array that is containing the probability of a given region containing text.
    :param min_confidence: threshold for ignoring areas that do not have sufficiently high probability.
    """
    (num_rows, num_cols) = scores.shape[2:4]
    detections = []
    confidences = []
    for y in range(0, num_rows):
        scores_data = scores[0, 0, y]
        x_data0 = geometry[0, 0, y]
        x_data1 = geometry[0, 1, y]
        x_data2 = geometry[0, 2, y]
        x_data3 = geometry[0, 3, y]
        angles_data = geometry[0, 4, y]
        for x in range(0, num_cols):
            if scores_data[x] < min_confidence:
                continue
            (offset_x, offset_y) = (x * 4.0, y * 4.0)
            angle = angles_data[x]
            cos = np.cos(angle)
            sin = np.sin(angle)
            h = x_data0[x] + x_data2[x]
            w = x_data1[x] + x_data3[x]
            end_x = int(offset_x + (cos * x_data1[x]) + (sin * x_data2[x]))
            end_y = int(offset_y - (sin * x_data1[x]) + (cos * x_data2[x]))
            start_x = int(end_x - w)
            start_y = int(end_y - h)
            detections.append((start_x, start_y, end_x, end_y))
            confidences.append(scores_data[x])
    return [detections, confidences]


def process_detection(image):
    """
    Process the EAST detection.
    :return: list with the bounding box coordinates for the found text regions. Every box defined as
             [start_x, start_y, end_x, end_y].
    """
    try:
        image = np.asarray(bytearray(image.stream.read()), dtype="uint8")
    except Exception:
        raise TextDetectException("Unable to open image file")
    image = cv2.imdecode(image, cv2.IMREAD_COLOR)
    if image is None:
        raise TextDetectException("Image buffer is too short or contains invalid data")
    (H, W) = image.shape[:2]
    ratio_width = W / float(_BASE_INPUT_SIZE)
    ratio_height = H / float(_BASE_INPUT_SIZE)
    blob = _prepare_image(image)
    layer_names = ["feature_fusion/Conv_7/Sigmoid", "feature_fusion/concat_3"]
    net = cv2.dnn.readNet("./app/frozen_east_text_detection.pb")
    net.setInput(blob)
    try:
        (scores, geometry) = net.forward(layer_names)
    except Exception:
        raise TextDetectException("Error occurred during text detection algorithm work")
    (detections, confidences) = _decode(scores, geometry, 0.5)
    boxes = non_max_suppression(np.array(detections), probs=confidences).astype(np.float)
    boxes[:, 0:4:2] *= ratio_width
    boxes[:, 1:4:2] *= ratio_height
    boxes = boxes.astype(int).tolist()
    return boxes
