from app import app
from flask import request, jsonify, make_response, abort
from app import text_detection
import traceback
import werkzeug


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)


@app.errorhandler(400)
def not_found(error):
    return make_response(jsonify({'error': 'Bad request'}), 400)


@app.errorhandler(Exception)
def exception_handler(error):
    data = {'error': 'Internal server error'}
    if type(error) in [text_detection.TextDetectException, werkzeug.exceptions.RequestEntityTooLarge]:
        data['message'] = str(error)
    app.logger.error(error)
    app.logger.error(traceback.format_exc())
    return make_response(jsonify(data), 500)


@app.route('/process', methods=['POST'])
def process():
    image = request.files.get('image')
    if image is None:
        abort(400)
    result = text_detection.process_detection(image)
    return jsonify(result)
